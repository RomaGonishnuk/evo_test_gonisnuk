class App
  def base_page
    @base_page ||= BasePage.new
  end

  def list_page
    @list_page ||= ListPage.new
  end

  def favourite_page
    @favourite_page ||= FavouritePage.new
  end

  def item_page
    @item_page ||= ItemPage.new
  end

end