class FirstItemGallery<SitePrism::Section
  element :favorite_icon, '.b-favorites-icon__holder .b-iconed-text.js-comparison-handler'
  element :favorite_text, '.b-pseudo-link', text: 'Добавить в избранное'
  element :item_name, '.b-product-gallery__product-name-link.qa-product-name-link'
end

class FirstItemList < SitePrism::Section

  element :favorite_icon, '.b-iconed-text__icon-holder.h-vertical-middle.js-comparison-handler-star',  match: :first
  element :item_name, '.b-product-list__name.qa-product-name-link'

end

class ListPage < SitePrism::Page

  set_url "#{CNFG['environments']["#{ENV['SERVER']}"]['url']}" +'/{section}{?query*}'

  element :list_sort, '#output_view_mode_0'
  element :gallery_sort, '#output_view_mode_1'
  element :favourite_sort_icon, '.b-head-control-panel__link-counter'

  element :gallery_active_select, '.b-button.qa-gallery-view-button.b-button_state_active .icon-gallery_mode.h-mh-12.h-vertical-middle'
  element :list_active_select  ,  '.b-button.qa-list-view-button.b-button_state_active .icon-list_mode.h-mh-12.h-vertical-middle'

  section :first_item_gallery, FirstItemGallery, '.b-product-gallery__content', match: :first
  section :first_item_list, FirstItemList, '.b-product-list.b-hovered.qa-product-block.js-rtb-partner', match: :first

end