class ItemPage < SitePrism::Page

  element :main_favourite_icon_inactive, ' .b-product-images__main-img .h-vertical-middle.h-cursor-pointer.h-select-none.qa-comparison-star.icon-comparison'
  element :scroll_down_favourite_icon_active, '.b-product__sidebar-movable.qa-sidebar-right-block .h-vertical-middle.h-cursor-pointer.h-select-none.qa-comparison-star.icon-comparison-active'
  element :bottom_desc, '.qa-product-company-opinions'
  element :search_field, '#search_text'

  set_url "#{CNFG['environments']["#{ENV['SERVER']}"]['url for item']}"

end