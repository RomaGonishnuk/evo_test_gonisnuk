class BasePage < SitePrism::Page

  set_url "#{CNFG['environments']["#{ENV['SERVER']}"]['url']}"

end