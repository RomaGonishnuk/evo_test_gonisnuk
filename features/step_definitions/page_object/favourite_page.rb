class MailSentPopup < SitePrism::Section

  element :input_field, '#comparison-email'
  element :submit_button, '#submit_button'

end

class FavouritePage < SitePrism::Page

  set_url "#{CNFG['environments']["#{ENV['SERVER']}"]['url']}" + '/comparison/list'

  element :favourite_counter, '[href="/comparison/list"] span'
  element :first_favourite_item_desc, '.b-items-table__cell:nth-child(1) div div:nth-child(2) a', match: :first
  element :mail_sent_button, '#comparison_send_to_email'
  element :remove_first_item, '.b-products-compare-list__closer-button.js-remove-from-comparison', match: :first
  element :empty_favoutrite_cart_state, '.b-info-panel.b-info-panel_border-dashed_grey.h-inline-block.h-bg-none'


  section :mail_sent, MailSentPopup, '.js-send_to_email-overlay.b-overlay.b-overlay_type_dialog.h-width-290.h-hidden'


  def sent_mail
    mail_sent.input_field.set CNFG['environments']["#{ENV['SERVER']}"]['test_mail']
    mail_sent.submit_button.click

  end
end