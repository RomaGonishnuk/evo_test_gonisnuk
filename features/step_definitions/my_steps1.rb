
Given(/^i at home page$/) do
  @app.base_page.load
end

Then(/^i navigate to given "([^"]*)" section with "([^"]*)" grid$/) do |sect, phyl|
  @app.list_page.load(section: "#{sect}", query: {output: "#{phyl}"})

end

And(/^Check sorting filter as gallery position$/) do
  @app.list_page.has_gallery_active_select?
end

When(/^i add item to favorites$/) do
  @app.list_page.find('.b-product-gallery__content', match: :first).hover
  @app.list_page.first_item_gallery.favorite_text.click

  TEST["item_name"]["added tot favorite name as gallery"] = @app.list_page.first_item_gallery.item_name.text

  File.open('config/saved_test_data.yml', 'w') do |h|
    h.write   TEST.to_yaml
  end

end

Then(/^i should check counter in navigation bar$/) do
  @app.list_page.has_favourite_sort_icon? text: "1"
end

And(/^i should check succesfull adding to Favorit list as "([^"]*)" way$/) do |way|
  @app.favourite_page.load
  @app.favourite_page.has_favourite_counter? text: "1"
  expect(@app.favourite_page.first_favourite_item_desc.text).to eq "#{TEST["item_name"]["#{way}"]}"
end


And(/^Check sorting filter as list position$/) do
  @app.list_page.has_list_active_select?
end

When(/^i add item to favorites as list grid$/) do
  @app.list_page.find('.b-product-list.b-hovered.qa-product-block.js-rtb-partner', match: :first).hover
  @app.list_page.first_item_list.favorite_icon.click

  TEST["item_name"]["added tot favorite name as list"] = @app.list_page.first_item_list.item_name.text

  File.open('config/saved_test_data.yml', 'w') do |h|
    h.write   TEST.to_yaml
  end
end

Given(/^i go to item as given link$/) do
  @app.item_page.load
end

Then(/^i add item to favourite list at top description section$/) do
  @app.item_page.main_favourite_icon_inactive.click

  TEST["item_name"]["added as self profile"] = @app.item_page.search_field['value']

  File.open('config/saved_test_data.yml', 'w') do |h|
    h.write   TEST.to_yaml
  end
end

And(/^i scroll down and verify at float description section$/) do
  @app.item_page.bottom_desc.click
  @app.item_page.has_scroll_down_favourite_icon_active?
end

Then(/^i send mail$/) do
  @app.favourite_page.mail_sent_button.click
  @app.favourite_page.sent_mail
end

And(/^remove item from favourites list$/) do
  @app.favourite_page.wait_for_remove_first_item
  @app.favourite_page.remove_first_item.click
  @app.favourite_page.has_empty_favoutrite_cart_state?
end