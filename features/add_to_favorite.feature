Feature: Add item to Favorites as unregistered user

  Scenario: Add item to favorites with gallery GUI
   # Given i at home page
    Then i navigate to given "Velosipednye-shiny" section with "gallery" grid
    And Check sorting filter as gallery position
    When i add item to favorites
    Then i should check counter in navigation bar
    And i should check succesfull adding to Favorit list as "added tot favorite name as gallery" way

   Scenario: Add item to favorites with list GUI
        # Given i at home page
     Then i navigate to given "Velosipednye-shiny" section with "list" grid
     And Check sorting filter as list position
     When i add item to favorites as list grid
     Then i should check counter in navigation bar
     And i should check succesfull adding to Favorit list as "added tot favorite name as list" way

    Scenario: Add item to favorites with direct link
      Given i go to item as given link
      Then i add item to favourite list at top description section
      And i scroll down and verify at float description section
      Then i should check counter in navigation bar
      And i should check succesfull adding to Favorit list as "added as self profile" way
      Then i send mail
      And remove item from favourites list

    Scenario: Failed scenario(Check reprot at reports/reports.html)
      Given i at home page
      Then i should check counter in navigation bar


