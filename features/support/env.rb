require 'selenium-webdriver'
require 'site_prism'
include SitePrism
require 'capybara/dsl'
require_relative "common_methods"
require 'capybara'


CNFG = YAML.load_file('config/test_data.yml')
TEST = YAML.load_file('config/saved_test_data.yml')


SitePrism.configure do |config|
  config.use_implicit_waits = true
end


Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new( app, browser: :chrome )
end

Capybara.register_driver :firefox do |app|
  Capybara::Selenium::Driver.new app
end


ENV['browser'] ||= 'firefox' # если фолс, то фаерфокс

#Capybara.javascript_driver = :chrome
Capybara.default_driver =  get_browser.to_sym
Capybara.default_selector = :css
Capybara.current_session.driver.browser.manage.window.maximize
