
Before do
  Capybara.reset_sessions!#resets cash&cookies before scenario
  @app = App.new
end



After do | scenario |
  if scenario.failed?
    scr = "#{scenario.__id__}.png" #__ - Ruby methods

    Capybara.page.driver.browser.save_screenshot("reports/#{scr}")
    embed("reports/#{scr}", 'image/png', 'SCREENSHOT')
  end
end
