
def get_browser
  if %W(chrome firefox).include?(ENV['browser'])
    ENV['browser']
  else
    'firefox'
  end
end